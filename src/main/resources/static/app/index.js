$(".phone-details-link").click(function (event) {
    var target = $(event.target);

    $('#phoneDetailsModalCenterTitle').text(target.data('identifier'));

    //A spinner would be great for UX

    $('#phoneDetailsModal').modal('show');

    //TODO complete handling
    $.ajax({
        url: target.data('url'),
        method: "GET",
        success: function (result) {
            $('#phoneDetailsModalBody').text(result); //TODO extract data from json
        },
        error: function () {
            $('#phoneDetailsModalBody').text("Error occurred while retrieving phone details");
        }
    });
});

$(".book-action-button").click(function (event) {
    var target = $(event.target);

    $.ajax({
        url: target.data('url'),
        method: "POST",
        success: function (result) {
            window.location.reload();
        },
        error: function () {
            if(!alert('Update action failed. Page will refresh!')){window.location.reload();}
        }
    });
});