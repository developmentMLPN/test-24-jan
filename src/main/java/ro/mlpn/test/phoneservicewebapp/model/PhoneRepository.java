package ro.mlpn.test.phoneservicewebapp.model;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PhoneRepository {

    List<Phone> findAll();

    Optional<Phone> getById(UUID phoneId);
}
