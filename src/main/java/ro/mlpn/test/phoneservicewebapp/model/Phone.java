package ro.mlpn.test.phoneservicewebapp.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Setter(AccessLevel.NONE)
@Builder
public class Phone {

    private UUID id;

    private String brand;

    private String model;

    private String bookedBy;

    private LocalDateTime bookedAt; //instant more appropriate (cutting corners)

    //in real world scenarios (SQL) the locking would ne provided by optimistic locking

    public void book(String bookingUser, LocalDateTime bookingTime) {
        synchronized (this) {
            if (bookedBy != null) {
                throw new IllegalStateException("Phone already booked by " + bookedBy);
            }
            bookedBy = bookingUser;
            bookedAt = bookingTime;
        }
    }

    public void bookOut(String bookingOutUser) {
        synchronized (this) {
            if (bookedBy == null || !bookedBy.equals(bookingOutUser)) {
                throw new IllegalStateException("Phone is not booked by " + bookingOutUser);
            }
            bookedBy = null;
            bookedAt = null;
        }
    }

    public boolean isBooked() {
        return bookedBy != null;
    }
}
