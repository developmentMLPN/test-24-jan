package ro.mlpn.test.phoneservicewebapp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ro.mlpn.test.phoneservicewebapp.api.PhoneDetailsUI;
import ro.mlpn.test.phoneservicewebapp.api.PhoneUI;
import ro.mlpn.test.phoneservicewebapp.model.Phone;
import ro.mlpn.test.phoneservicewebapp.model.PhoneRepository;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PhoneService {

    private PhoneRepository phoneRepository;

    private PhoneModelToUIConverter uiConverter;

    private PhoneDetailsProvider phoneDetailsProvider;

    private Clock clock;

    public List<PhoneUI> getAllPhones() {
        return phoneRepository.findAll().stream()
                .map(uiConverter::convert)
                .collect(Collectors.toList());
    }

    public PhoneDetailsUI getPhoneDetails(UUID phoneId) {
        Phone phone = getPhoneById(phoneId);
        return phoneDetailsProvider.getPhoneDetails(phone.getBrand(), phone.getModel());
    }

    public void bookPhone(UUID phoneId, String user) {
        Phone phone = getPhoneById(phoneId);
        phone.book(user, LocalDateTime.now(clock));
    }

    public void bookOutPhone(UUID phoneId, String user) {
        Phone phone = getPhoneById(phoneId);
        phone.bookOut(user);
    }

    private Phone getPhoneById(UUID phoneId) {
        return phoneRepository.getById(phoneId).orElseThrow(() -> new IllegalStateException("Unknown phone " + phoneId));
    }
}
