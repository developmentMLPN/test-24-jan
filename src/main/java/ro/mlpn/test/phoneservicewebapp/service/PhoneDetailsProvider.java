package ro.mlpn.test.phoneservicewebapp.service;

import ro.mlpn.test.phoneservicewebapp.api.PhoneDetailsUI;

public interface PhoneDetailsProvider {

    PhoneDetailsUI getPhoneDetails(String brand, String model);
}
