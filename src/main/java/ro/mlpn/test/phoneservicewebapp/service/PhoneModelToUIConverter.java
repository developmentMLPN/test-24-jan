package ro.mlpn.test.phoneservicewebapp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;
import ro.mlpn.test.phoneservicewebapp.Routes;
import ro.mlpn.test.phoneservicewebapp.configuration.SystemConfigurationProperties;
import ro.mlpn.test.phoneservicewebapp.model.Phone;
import ro.mlpn.test.phoneservicewebapp.api.PhoneUI;

import java.util.UUID;

@Component
@AllArgsConstructor
public class PhoneModelToUIConverter {

    private SystemConfigurationProperties systemConfigurationProperties;

    public PhoneUI convert(Phone phone) {
        UUID phoneId = phone.getId();

        PhoneUI.PhoneUIBuilder builder = PhoneUI.builder()
                .id(phoneId)
                .brand(phone.getBrand())
                .model(phone.getModel())

                .detailsUrl(buildDetailsURL(phoneId));

        if (phone.isBooked()) {
            builder
                .bookedBy(phone.getBookedBy())
                .bookedAt(phone.getBookedAt())
                .bookingOutUrl(buildBookingOutURL(phoneId));
        } else {
            builder
                .bookingUrl(buildBookingURL(phoneId));
        }

        return builder.build();
    }

    private String buildDetailsURL(UUID id) {
        return UriComponentsBuilder.fromHttpUrl(systemConfigurationProperties.getApplicationBaseUrl())
                .path(Routes.PHONE_DETAILS)
                .buildAndExpand(id).toUriString();
    }

    private String buildBookingURL(UUID id) {
        return UriComponentsBuilder.fromHttpUrl(systemConfigurationProperties.getApplicationBaseUrl())
                .path(Routes.PHONE_BOOK)
                .buildAndExpand(id).toUriString();
    }

    private String buildBookingOutURL(UUID id) {
        return UriComponentsBuilder.fromHttpUrl(systemConfigurationProperties.getApplicationBaseUrl())
                .path(Routes.PHONE_BOOKOUT)
                .buildAndExpand(id).toUriString();
    }
}