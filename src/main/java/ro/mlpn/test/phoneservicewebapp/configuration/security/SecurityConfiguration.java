package ro.mlpn.test.phoneservicewebapp.configuration.security;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;
import ro.mlpn.test.phoneservicewebapp.Routes;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    public static final String AUTHENTICATION_COOKIE_NAME = "auth";

    private TrustedUsernameAuthenticationProvider authenticationProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
            .addFilterBefore(new CookieBasedAuthenticationFilter(AUTHENTICATION_COOKIE_NAME), UsernamePasswordAuthenticationFilter.class)
            .authorizeRequests()
                .antMatchers("/static/**").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage(Routes.LOGIN)
                .successHandler(new CookieSetterAuthenticationSuccessHandler(AUTHENTICATION_COOKIE_NAME))
                .permitAll()
                .and()
            .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher(Routes.LOGOUT, HttpMethod.GET.name()))
                .deleteCookies(AUTHENTICATION_COOKIE_NAME)
                .logoutSuccessUrl(Routes.LOGIN)
                .permitAll()
                .and()
            .httpBasic()
                .disable()
            .rememberMe()
                .disable()
            .csrf()
                .disable();
        // @formatter:on
    }

    @AllArgsConstructor
    private static class CookieSetterAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

        private String cookieName;

        @Override
        public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
            //this would normally be a JWT session cookie but i have to cut corners due to time constraints . Sorry .

            Cookie cookie = new Cookie(cookieName, (String) authentication.getPrincipal());
            cookie.setHttpOnly(true);
            response.addCookie(cookie);

            response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
            response.setHeader(HttpHeaders.LOCATION, Routes.INDEX);
        }
    }

    @AllArgsConstructor
    private static class CookieBasedAuthenticationFilter extends OncePerRequestFilter {

        private String cookieName;

        @Override
        protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
            Cookie authenticationCookie = WebUtils.getCookie(httpServletRequest, cookieName);

            if (authenticationCookie != null) {
                //this would normally validate the JWT and only authenticate if JWT is not tampered with and valid

                String username = authenticationCookie.getValue();
                SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(username, null, AuthorityUtils.NO_AUTHORITIES));
            }

            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }
}
