package ro.mlpn.test.phoneservicewebapp.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import ro.mlpn.test.phoneservicewebapp.integrations.FonoapiPhoneDetailsProvider;
import ro.mlpn.test.phoneservicewebapp.model.Phone;
import ro.mlpn.test.phoneservicewebapp.model.PhoneRepository;
import ro.mlpn.test.phoneservicewebapp.service.PhoneDetailsProvider;

import java.time.Clock;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Configuration
@EnableCaching
@EnableConfigurationProperties(SystemConfigurationProperties.class)
public class ApplicationConfiguration {

    public static final String PHONE_DETAILS_CACHE = "phone-details";

    @Bean
    public PhoneRepository phoneRepository() {
        List<Phone> phones = new ArrayList<>();
        phones.add(newPhone("Samsung", "Galaxy S9"));
        phones.add(newPhone("Samsung", "Galaxy S8"));
        phones.add(newPhone("Samsung", "Galaxy S7"));
        phones.add(newPhone("Motorola", "Nexus 5"));
        phones.add(newPhone("LG", "Nexus 5X"));
        phones.add(newPhone("Huawei", "Honor 7X"));
        phones.add(newPhone("Apple", "iPhone X"));
        phones.add(newPhone("Apple", "iPhone 8"));
        phones.add(newPhone("Apple", "iPhone 4s"));
        phones.add(newPhone("Nokia", "3310"));

        return new InMemoryPhoneRepository(phones);
    }

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(PHONE_DETAILS_CACHE);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(2))
                .setReadTimeout(Duration.ofSeconds(2)).build();
    }

    @Bean
    public PhoneDetailsProvider phoneDetailsProvider(RestTemplate restTemplate, SystemConfigurationProperties configurationProperties) {
        return new FonoapiPhoneDetailsProvider(restTemplate, configurationProperties.getFonoApiToken());
    }

    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

    private Phone newPhone(String brand, String model) {
        return Phone.builder()
                .id(UUID.randomUUID())
                .brand(brand)
                .model(model).build();
    }
}
