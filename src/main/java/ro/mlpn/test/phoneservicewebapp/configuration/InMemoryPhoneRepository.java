package ro.mlpn.test.phoneservicewebapp.configuration;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import ro.mlpn.test.phoneservicewebapp.model.Phone;
import ro.mlpn.test.phoneservicewebapp.model.PhoneRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
class InMemoryPhoneRepository implements PhoneRepository {

    @NonNull
    List<Phone> phones;

    @Override
    public List<Phone> findAll() {
        return phones;
    }

    @Override
    public Optional<Phone> getById(UUID phoneId) {
        return phones.stream()
                .filter(p -> p.getId().equals(phoneId))
                .findFirst();
    }
}
