package ro.mlpn.test.phoneservicewebapp.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "app")
public class SystemConfigurationProperties {

    private String applicationBaseUrl;

    private String fonoApiToken;
}
