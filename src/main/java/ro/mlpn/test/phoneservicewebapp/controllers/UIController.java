package ro.mlpn.test.phoneservicewebapp.controllers;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ro.mlpn.test.phoneservicewebapp.service.PhoneService;

import static ro.mlpn.test.phoneservicewebapp.Routes.INDEX;
import static ro.mlpn.test.phoneservicewebapp.Routes.LOGIN;

@Controller
@AllArgsConstructor
public class UIController {

    private PhoneService phoneService;

    @GetMapping(INDEX)
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("phones", phoneService.getAllPhones());
        return modelAndView;
    }

    @GetMapping(LOGIN)
    public ModelAndView login() {
        return new ModelAndView("login");
    }
}
