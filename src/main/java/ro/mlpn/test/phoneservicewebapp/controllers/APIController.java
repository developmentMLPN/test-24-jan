package ro.mlpn.test.phoneservicewebapp.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.mlpn.test.phoneservicewebapp.Routes;
import ro.mlpn.test.phoneservicewebapp.api.PhoneDetailsUI;
import ro.mlpn.test.phoneservicewebapp.service.PhoneService;

import java.util.UUID;

@RestController
@Slf4j
@AllArgsConstructor
public class APIController {

    private PhoneService phoneService;

    @GetMapping(Routes.PHONE_DETAILS)
    public PhoneDetailsUI getPhoneDetails(@PathVariable(Routes.PUBLIC_IDENTIFIER) UUID phoneId, @AuthenticationPrincipal Authentication authenticatedUser) {
        log.info("User {} requested phone details for {}", authenticatedUser.getPrincipal(), phoneId);
        return phoneService.getPhoneDetails(phoneId);
    }

    @PostMapping(Routes.PHONE_BOOK)
    public void bookPhone(@PathVariable(Routes.PUBLIC_IDENTIFIER) UUID phoneId, @AuthenticationPrincipal Authentication authenticatedUser) {
        log.info("User {} requesting to book phone {}", authenticatedUser.getPrincipal(), phoneId);
        phoneService.bookPhone(phoneId, authenticatedUser.getPrincipal().toString());
    }

    @PostMapping(Routes.PHONE_BOOKOUT)
    public void bookOutPhone(@PathVariable(Routes.PUBLIC_IDENTIFIER) UUID phoneId, @AuthenticationPrincipal Authentication authenticatedUser) {
        log.info("User {} requesting to book out phone {}", authenticatedUser.getPrincipal(), phoneId);
        phoneService.bookOutPhone(phoneId, authenticatedUser.getPrincipal().toString());
    }
}
