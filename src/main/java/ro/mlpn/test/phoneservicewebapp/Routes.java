package ro.mlpn.test.phoneservicewebapp;

public class Routes {

    public static final String PUBLIC_IDENTIFIER = "publicIdentifier";

    public static final String INDEX = "/";
    public static final String LOGIN = "/login";
    public static final String LOGOUT = "/logout";

    public static final String PHONE_DETAILS    = "/api/phones/{" + PUBLIC_IDENTIFIER + "}/details";

    //Based on https://cloud.google.com/apis/design/custom_methods system
    public static final String PHONE_BOOK       = "/api/phones/{" + PUBLIC_IDENTIFIER + "}:book";
    public static final String PHONE_BOOKOUT    = "/api/phones/{" + PUBLIC_IDENTIFIER + "}:book-out";
}
