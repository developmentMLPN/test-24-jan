package ro.mlpn.test.phoneservicewebapp.integrations;

import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ro.mlpn.test.phoneservicewebapp.api.PhoneDetailsUI;
import ro.mlpn.test.phoneservicewebapp.configuration.ApplicationConfiguration;
import ro.mlpn.test.phoneservicewebapp.service.PhoneDetailsProvider;

@AllArgsConstructor
public class FonoapiPhoneDetailsProvider implements PhoneDetailsProvider {

    private static final String DEVICE_DETAILS_BASE_URL = "https://fonoapi.freshpixl.com/v1/getdevice";

    private RestTemplate restTemplate;

    private String token;

    @Cacheable(ApplicationConfiguration.PHONE_DETAILS_CACHE)
    @Override
    public PhoneDetailsUI getPhoneDetails(String brand, String model) {

        String phoneDetailsUrl = UriComponentsBuilder
                .fromHttpUrl(DEVICE_DETAILS_BASE_URL)
                .queryParam("token", token)
                .queryParam("brand", brand)
                .queryParam("device", model)
                .queryParam("position", 0) // get the most relevant result
                .build().toUriString();

        FonoapiDeviceResult rawResponse = restTemplate.getForObject(phoneDetailsUrl, FonoapiDeviceResult.class);

        return PhoneDetailsUI.builder()
                .technology(rawResponse.getTechnology())
                .twoGBands(rawResponse.get_2g_bands())
                .threeGBands(rawResponse.get_3g_bands())
                .fourGBands(rawResponse.get_4g_bands()).build();
    }
}
