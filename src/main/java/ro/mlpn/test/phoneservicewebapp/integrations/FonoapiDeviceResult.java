package ro.mlpn.test.phoneservicewebapp.integrations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class FonoapiDeviceResult {

    private String technology;

    private String _2g_bands;

    private String _3g_bands;

    private String _4g_bands;
}
