package ro.mlpn.test.phoneservicewebapp.api;

import lombok.*;

@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class PhoneDetailsUI {

    private String technology;

    private String twoGBands;

    private String threeGBands;

    private String fourGBands;
}
