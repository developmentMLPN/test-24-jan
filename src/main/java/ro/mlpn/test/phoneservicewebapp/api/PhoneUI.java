package ro.mlpn.test.phoneservicewebapp.api;

import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
public class PhoneUI {

    @NonNull
    private UUID id;

    @NonNull
    private String brand;

    @NonNull
    private String model;

    private String bookedBy;

    private LocalDateTime bookedAt;

    @NonNull
    private String detailsUrl;

    private String bookingUrl;

    private String bookingOutUrl;
}
