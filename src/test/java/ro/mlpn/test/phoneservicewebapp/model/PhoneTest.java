package ro.mlpn.test.phoneservicewebapp.model;

import org.junit.Test;

import java.time.LocalDateTime;
import java.util.UUID;

public class PhoneTest {

    @Test(expected = IllegalStateException.class)
    public void bookingAnAlreadyBookedPhoneShouldNotWork() {
        Phone phone = buildDummyPhone(true);
        phone.book("a", LocalDateTime.now());
    }

    @Test(expected = IllegalStateException.class)
    public void bookingOutAPhoneThatIsNotBookedShouldNotWork() {
        Phone phone = buildDummyPhone(false);
        phone.bookOut("a");
    }

    @Test(expected = IllegalStateException.class)
    public void bookingOutAPhoneThatIsNotBookedByCurrentUserShouldNotWork() {
        Phone phone = buildDummyPhone(true);
        phone.bookOut("a");
    }

    @Test
    public void bookingAndBookingOutShouldWork() {
        Phone phone = buildDummyPhone(false);
        phone.book("a", LocalDateTime.now());
        phone.bookOut("a");
    }


    private Phone buildDummyPhone(boolean booked) {
        Phone.PhoneBuilder builder = Phone.builder()
                .id(UUID.randomUUID())
                .brand("B")
                .model("M");
        if (booked) {
            builder
                    .bookedBy("u")
                    .bookedAt(LocalDateTime.now());
        }
        return builder.build();
    }
}