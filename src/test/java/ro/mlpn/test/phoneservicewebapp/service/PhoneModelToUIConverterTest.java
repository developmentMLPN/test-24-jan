package ro.mlpn.test.phoneservicewebapp.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.util.UriComponentsBuilder;
import ro.mlpn.test.phoneservicewebapp.Routes;
import ro.mlpn.test.phoneservicewebapp.configuration.SystemConfigurationProperties;
import ro.mlpn.test.phoneservicewebapp.model.Phone;
import ro.mlpn.test.phoneservicewebapp.api.PhoneUI;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PhoneModelToUIConverterTest {

    private static final UUID PHONE_ID = UUID.randomUUID();
    private static final String BRAND = "b";
    private static final String MODEL = "m";
    private static final String BOOKING_USER = "u";
    private static final LocalDateTime BOOKED_AT = LocalDateTime.now();

    @Mock
    private SystemConfigurationProperties systemConfigurationProperties;

    private PhoneModelToUIConverter converter;

    @Before
    public void setUp() throws Exception {
        Mockito.when(systemConfigurationProperties.getApplicationBaseUrl()).thenReturn("http://some-url.test");
        converter = new PhoneModelToUIConverter(systemConfigurationProperties);
    }

    @Test
    public void notBookedPhoneShouldBeCorrectlyConverted() {
        Phone phone = Phone.builder()
                .id(PHONE_ID)
                .brand(BRAND)
                .model(MODEL).build();

        String bookPhoneUrl = UriComponentsBuilder.fromHttpUrl(systemConfigurationProperties.getApplicationBaseUrl())
                .path(Routes.PHONE_BOOK)
                .buildAndExpand(PHONE_ID).toUriString();

        PhoneUI expected = PhoneUI.builder()
                .id(PHONE_ID)
                .brand(BRAND)
                .model(MODEL)
                .bookingUrl(bookPhoneUrl)
                .detailsUrl(buildExpectedDetailsUrl()).build();

        assertEquals(expected, converter.convert(phone));
    }

    @Test
    public void bookedPhonesShouldReturnDetailsAndBookingOutURL() {
        Phone phone = Phone.builder()
                .id(PHONE_ID)
                .brand(BRAND)
                .model(MODEL)
                .bookedBy(BOOKING_USER)
                .bookedAt(BOOKED_AT).build();

        String bookedOutUrl = UriComponentsBuilder.fromHttpUrl(systemConfigurationProperties.getApplicationBaseUrl())
                .path(Routes.PHONE_BOOKOUT)
                .buildAndExpand(PHONE_ID).toUriString();

        PhoneUI expected = PhoneUI.builder()
                .id(PHONE_ID)
                .brand(BRAND)
                .model(MODEL)
                .bookedBy(BOOKING_USER)
                .bookedAt(BOOKED_AT)
                .bookingOutUrl(bookedOutUrl)
                .detailsUrl(buildExpectedDetailsUrl()).build();


        assertEquals(expected, converter.convert(phone));
    }

    private String buildExpectedDetailsUrl() {
        return UriComponentsBuilder.fromHttpUrl(systemConfigurationProperties.getApplicationBaseUrl())
                .path(Routes.PHONE_DETAILS)
                .buildAndExpand(PHONE_ID).toUriString();
    }
}