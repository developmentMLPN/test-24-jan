package ro.mlpn.test.phoneservicewebapp.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ro.mlpn.test.phoneservicewebapp.model.Phone;
import ro.mlpn.test.phoneservicewebapp.model.PhoneRepository;
import ro.mlpn.test.phoneservicewebapp.api.PhoneUI;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhoneServiceTest {

    @Mock
    private PhoneRepository phoneRepository;

    @Mock
    private PhoneModelToUIConverter uiConverter;

    @Mock
    private PhoneDetailsProvider phoneDetailsProvider;

    private Clock clock = Clock.fixed(Instant.now(), ZoneId.systemDefault());

    private PhoneService phoneService;

    @Before
    public void setUp() throws Exception {
        phoneService = new PhoneService(phoneRepository, uiConverter, phoneDetailsProvider, clock);
    }

    @Test
    public void retrievingAllPhonesShouldWorkCorrectly() {
        Phone phone1 = mock(Phone.class);
        Phone phone2 = mock(Phone.class);

        PhoneUI phoneUI1 = mock(PhoneUI.class);
        PhoneUI phoneUI2 = mock(PhoneUI.class);

        when(phoneRepository.findAll()).thenReturn(asList(phone1, phone2));
        when(uiConverter.convert(eq(phone1))).thenReturn(phoneUI1);
        when(uiConverter.convert(eq(phone2))).thenReturn(phoneUI2);

        assertThat(phoneService.getAllPhones()).containsExactly(phoneUI1, phoneUI2);
    }
}