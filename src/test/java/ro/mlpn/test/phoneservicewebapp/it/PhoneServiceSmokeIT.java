package ro.mlpn.test.phoneservicewebapp.it;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ro.mlpn.test.phoneservicewebapp.Routes;
import ro.mlpn.test.phoneservicewebapp.api.PhoneDetailsUI;
import ro.mlpn.test.phoneservicewebapp.configuration.security.SecurityConfiguration;
import ro.mlpn.test.phoneservicewebapp.integrations.FonoapiDeviceResult;
import ro.mlpn.test.phoneservicewebapp.model.Phone;
import ro.mlpn.test.phoneservicewebapp.model.PhoneRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PhoneServiceSmokeIT {

    private TestRestTemplate testRestTemplate = new TestRestTemplate();

    @LocalServerPort
    private int port;

    @Autowired
    private PhoneRepository phoneRepository;

    @Autowired
    private RestTemplate appRestTemplate;

    private MockRestServiceServer mockRestServiceServer;

    @Before
    public void setUp() throws Exception {
        mockRestServiceServer = MockRestServiceServer.createServer(appRestTemplate);
    }

    @Test
    public void bookingAndBookingOutAPhoneShouldWork() {
        String user = "u";

        Phone phone = getBookablePhone();

        String bookPhoneURL = UriComponentsBuilder.fromHttpUrl(getLocalhostURL())
                .path(Routes.PHONE_BOOK)
                .buildAndExpand(phone.getId()).toUriString();

        testRestTemplate.exchange(bookPhoneURL, HttpMethod.POST, authenticatedUser(user), Void.class);

        assertThat(phoneRepository.getById(phone.getId()))
                .isPresent().get()
                .matches(Phone::isBooked);

        String bookOutPhoneURL = UriComponentsBuilder.fromHttpUrl(getLocalhostURL())
                .path(Routes.PHONE_BOOKOUT)
                .buildAndExpand(phone.getId()).toUriString();

        testRestTemplate.exchange(bookOutPhoneURL, HttpMethod.POST, authenticatedUser(user), Void.class);

        assertThat(phoneRepository.getById(phone.getId()))
                .isPresent().get()
                .matches(p -> !p.isBooked());
    }

    @Test
    public void retrievingPhoneDetailsShouldWork() {
        String user = "u";
        Phone phone = getBookablePhone();

        FonoapiDeviceResult fonoapiDeviceResult = FonoapiDeviceResult.builder()
                .technology("t")
                ._2g_bands("2g")
                ._3g_bands("3g")
                ._4g_bands("4g").build();

        mockExpectedFonoapiResponse(fonoapiDeviceResult);

        String phoneDetailsURL = UriComponentsBuilder.fromHttpUrl(getLocalhostURL())
                .path(Routes.PHONE_DETAILS)
                .buildAndExpand(phone.getId()).toUriString();

        ResponseEntity<PhoneDetailsUI> result = testRestTemplate.exchange(phoneDetailsURL, HttpMethod.GET, authenticatedUser(user), PhoneDetailsUI.class);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(result.getBody())
                .isNotNull()
                .matches(p -> p.getFourGBands().equals(fonoapiDeviceResult.get_4g_bands()))
                .matches(p -> p.getThreeGBands().equals(fonoapiDeviceResult.get_3g_bands()))
                .matches(p -> p.getTwoGBands().equals(fonoapiDeviceResult.get_2g_bands()))
                .matches(p -> p.getTechnology().equals(fonoapiDeviceResult.getTechnology()));
    }

    private void mockExpectedFonoapiResponse(FonoapiDeviceResult fonoapiDeviceResult) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            mockRestServiceServer.expect(MockRestRequestMatchers.method(HttpMethod.GET))
                    .andRespond(withSuccess(objectMapper.writeValueAsString(fonoapiDeviceResult), MediaType.APPLICATION_JSON));
        } catch (JsonProcessingException e) {
            throw new RuntimeException();
        }
    }

    private Phone getBookablePhone() {
        return phoneRepository.findAll().stream()
                .filter(phone -> !phone.isBooked())
                .findFirst().orElseThrow(IllegalStateException::new);
    }

    private String getLocalhostURL() {
        return "http://localhost:" + port;
    }

    private HttpEntity authenticatedUser(String user) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.COOKIE, SecurityConfiguration.AUTHENTICATION_COOKIE_NAME + "=" + user);

        return new HttpEntity(null, headers);
    }
}
