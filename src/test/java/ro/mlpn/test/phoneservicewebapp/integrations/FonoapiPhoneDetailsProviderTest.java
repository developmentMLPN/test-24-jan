package ro.mlpn.test.phoneservicewebapp.integrations;

import javafx.beans.binding.Bindings;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ro.mlpn.test.phoneservicewebapp.api.PhoneDetailsUI;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FonoapiPhoneDetailsProviderTest {

    private static final String MOCK_TOKEN = "tkn";

    @Mock
    private RestTemplate restTemplate;

    private FonoapiPhoneDetailsProvider detailsProvider;

    @Before
    public void setUp() throws Exception {
        detailsProvider = new FonoapiPhoneDetailsProvider(restTemplate, MOCK_TOKEN);
    }

    @Test
    public void providingDetailsShouldWork() {
        String brand = "b";
        String model = "m";

        String expectedUrl = "https://fonoapi.freshpixl.com/v1/getdevice?token=tkn&brand=b&device=m&position=0";

        FonoapiDeviceResult rawResult = FonoapiDeviceResult.builder()
                .technology("t")
                ._2g_bands("2g")
                ._3g_bands("3g")
                ._4g_bands("4g").build();

        when(restTemplate.getForObject(eq(expectedUrl), eq(FonoapiDeviceResult.class))).thenReturn(rawResult);

        PhoneDetailsUI expectedResult = PhoneDetailsUI.builder()
                .technology(rawResult.getTechnology())
                .twoGBands(rawResult.get_2g_bands())
                .threeGBands(rawResult.get_3g_bands())
                .fourGBands(rawResult.get_4g_bands()).build();

        Assert.assertEquals(expectedResult, detailsProvider.getPhoneDetails(brand, model));
    }
}